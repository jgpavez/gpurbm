#Makefile for Restricted Boltzmann GPU

CC=nvcc
RBM_MAIN=testRbm.cu
RBM_SOURCES=rbm.cu RBMCorrectWeights.cu RBMinitKernels.cu  RBMstatus.cu  RBMstatusSmall.cu
RBM_SOURCES_DIR=$(addprefix RBM/,$(RBM_SOURCES))
EXTERN=random/random.cu reduction/reduction.cpp reduction/*.cu
EXTERN_SOURCES=$(addprefix extern/,$(EXTERN))
RBM_INCLUDES=-IRBM -Iutil
LDFLAGS=-lcurand
RBM_EXECUTABLE=rbm

DBN_MAIN=testDbn.cu
DBN_SOURCES_DIR=$(RBM_SOURCES_DIR)
DBN_INCLUDES=$(RBM_INCLUDES) -IDBN
DBN_EXECUTABLE=dbn


#all: $(SOURCES_DIR) $(EXECUTABLE)

$(RBM_EXECUTABLE):
	$(CC) $(LDFLAGS) $(RBM_MAIN) $(RBM_SOURCES_DIR) $(EXTERN_SOURCES) $(RBM_INCLUDES) -o $@

$(DBN_EXECUTABLE):
	$(CC) $(LDFLAGS) $(DBN_MAIN) $(DBN_SOURCES_DIR) $(EXTERN_SOURCES) $(DBN_INCLUDES) -o $@

run: $(EXECUTABLE)
	./$(EXECUTABLE)

clean:
	rm $(EXECUTABLE)
