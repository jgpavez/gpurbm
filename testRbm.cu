#define EPOCHS 10

#include <iostream>
#include "ManageTrainingData.h"
#include "RBM.h"
#include "extern/common/CudaInit.h"
#include "extern/memory/DeviceArray.h"
#include "extern/memory/HostMatrix.h"

using namespace std;
using namespace GPUMLib;


int main (int argc, char ** argv)
{
    int size = 0;
    if (argc > 1) size = atoi(argv[1]); 
    bool sparsity = 1;
    if (argc > 2) sparsity = atoi(argv[2]);
    cudafloat initialLearningRate = 0.3;
    cudafloat momentum = 0.3;
    bool useBinaryValuesVisibleReconstruction = false;
    cudafloat stdWeights = 0.01;
    bool proportionRandomValuesGenerated = 1;
    bool useSparsity = sparsity;
    cudafloat sparsityTarget = 0.1;
    cudafloat sparsityReg = 0.95;
    cudafloat sparsityCost = 0.0001;


    CudaDevice device;
	const char *fileName = "data/train-images-idx3-ubyte";
    const char *testName = "data/t10k-images-idx3-ubyte";
    ManageTrainingData trainingData;
    trainingData.loadTrainingData(fileName, size);
	HostMatrix<cudafloat> inputs = trainingData.Inputs();

    RBM rbm(trainingData.trainingSet._inputSize, 256, inputs, initialLearningRate, momentum,
    useBinaryValuesVisibleReconstruction, stdWeights, proportionRandomValuesGenerated, useSparsity, sparsityTarget,
    sparsityReg, sparsityCost);
    //RBM rbm(trainingData.trainingSet._inputSize, 256, inputs, 0.3);

    ManageTrainingData testData;
    testData.loadTrainingData(testName,(int) size );
    HostMatrix<cudafloat> test = testData.Inputs();
    rbm.SetValidationSet(testData.trainingSet._inputSize, test, 1);
   
    rbm.Init(50, 1);
    for (int i = 0; i < EPOCHS ; i++)
    rbm.ContrastiveDivergence(1);

	return 1;
}
