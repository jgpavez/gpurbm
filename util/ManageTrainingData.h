#ifndef RestrictedBoltzmannGPU_ManageTrainingData_h
#define RestrictedBoltzmannGPU_ManageTrainingData_h
#include <iostream>
#include <fstream>
#include "../extern/common/CudaDefinitions.h"
#include "../extern/memory/HostMatrix.h"

using namespace std;
using namespace GPUMLib;

class ManageTrainingData{
public:
	// Non-Intel formatting
	int swap4(int i) 
	{
    	unsigned char c1, c2, c3, c4;
    	c1 = i & 255;
    	c2 = (i >> 8) & 255;
    	c3 = (i >> 16) & 255;
    	c4 = (i >> 24) & 255;
		return ((int)c1 << 24) + ((int)c2 << 16) + ((int)c3 << 8) + c4;
	}

	struct _trainingSet{
		int _inputSize;
		int _numberRows;
		int _numberCols;
		int _trainingSize;
		HostMatrix<cudafloat> _data;
	};

	struct _trainingSet	trainingSet;
	ifstream *trainingData;
	ManageTrainingData(){};
	int loadTrainingData(const char *inputFile, int trainingSize)
	{
		trainingData = new ifstream;
		trainingData->open(inputFile, ios::binary);
		if (!trainingData->is_open()) return -1;

		
		// Magic Numbers
		int magicNumber = 0;
		trainingData->read((char*)&magicNumber, sizeof(magicNumber));
		magicNumber = swap4(magicNumber);

		// Number of images
		int numberImages = 0;
		trainingData->read((char*)&numberImages, sizeof(numberImages));
		numberImages = swap4(numberImages);
		trainingSet._trainingSize = numberImages;
        
        //Testing
        if (trainingSize > 0) trainingSet._trainingSize = trainingSize;

        //cout << "Number trainig cases: " << trainingSet._trainingSize<< endl;

   

		int numberRows = 0;
		trainingData->read((char*)&numberRows, sizeof(numberRows));
		numberRows = swap4(numberRows);
		trainingSet._numberRows = numberRows;

		// Number Cols
		int numberCols = 0;
		trainingData->read((char*)&numberCols, sizeof(numberCols));
		numberCols = swap4(numberCols);
		trainingSet._numberCols = numberCols;
		trainingSet._inputSize = numberCols * numberRows;
        trainingSet._data.ResizeWithoutPreservingData(trainingSet._trainingSize, trainingSet._inputSize);
		//trainingSet._data = new (nothrow) float[trainingSet._inputSize * trainingSet._trainingSize * sizeof(float)];

		// Training Set		
		for ( unsigned int i = 0; i < trainingSet._trainingSize; i++){
			for ( unsigned int j = 0; j < trainingSet._inputSize; j++ ){
				unsigned char temp = 0;
				trainingData->read((char*)&temp, sizeof(temp));
				trainingSet._data(i,j) = (cudafloat) temp / 255;
			}
		}


		trainingData->close();
		return 1;
	};
    HostMatrix<cudafloat> &Inputs() {
        return trainingSet._data;
    }

};

#endif
