#include "ART2.h"
#include "ManageTrainingData.h"
#include <iostream>
#include <cstdlib>

using namespace std;

int main (int argc, char **argv)
{
    int size = 0;
    if (argc > 1) size = atoi(argv[1]);
    int max_f2 = 10;
    double rho = 0.3;
	const char *fileName = "../data/train-images-idx3-ubyte";
    const char *testName = "../data/t10k-images-idx3-ubyte";
    ManageTrainingData trainingData;
    trainingData.loadTrainingData(fileName, size);
	double *inputs = trainingData.Inputs();

    ART1 *art1;
    art1 = Art1Alloc(trainingData.trainingSet._inputSize, max_f2, rho);

    for (int i = 0; i < size; i++){
        Art1Process(art1, &inputs[i * trainingData.trainingSet._inputSize]);
    }
    
    Art1Free(&art1);

    return 0;
}
